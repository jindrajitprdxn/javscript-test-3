/* Add click event*/
document.querySelector('.check-rgb').addEventListener("click", (e) => {
	e.preventDefault();
	testColorFormat();
})

/* Function to check the color format */
function testColorFormat() {
	document.querySelector('.error').innerText = "";
	document.querySelector('.result-string').innerText = "";
	let text = document.querySelector('.text-field').value;
	/*Regx to check enter rgb format valid or not*/
	let regx = /^rgba?\((\d+),\s(\d+),\s(\d+)\)/;
	if(text === "") {
		document.querySelector('.error').innerText = "This field is manditory";
	} else if(!text.match(regx)) {
		document.querySelector('.error').innerText = "Please enter valid rgb(a) format";
	} else {
		let firstIndex = text.indexOf('(');
		let secondIndex = text.indexOf(')');
		let rgbValue = text.slice(firstIndex + 1,secondIndex);
		rgbValue = rgbValue.split(', ');
		rgbValue.map(value => {
			if(value < 0 || value > 255) {
				document.querySelector('.result-string').innerText = false;
			} else {
				document.querySelector('.result-string').innerText = true;
			}
		})
	}
}